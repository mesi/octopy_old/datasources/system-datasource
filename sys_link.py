from lib.links.base_link import BaseLink


class SysLink(BaseLink):
    def deserialize(self, conf):
        super().deserialize(conf)
        self.link_type = 'sys'
