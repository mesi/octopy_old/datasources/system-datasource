#!/usr/bin/python3
import sys
import time
import json
import logging
from os import path
import argparse
import subprocess
import re
import psutil
from lib.octopyapp.octopyapp import OctopyApp
from lib.links import links
from sys_link import SysLink

APP_ID = 'System Datasource'
LOG_LEVEL = logging.INFO

STATUS_PUBLISH_INTERVAL = 2
FAST_UPDATE_INTERVAL = 1
SLOW_UPDATE_INTERVAL = 10
DATA_PUBLISH_INTERVAL = 1


class SystemDatasource(OctopyApp):
    def __init__(self, config):
        super().__init__(APP_ID, LOG_LEVEL, config)
        self.id = 'system'
        self.links = {}
        self.source_prefix = path.join(self.config['mqtt']['prefix'], self.id)
        self.temperatures = {}
        self.ntp_server = None
        self.create_links()


    def start(self):
        super().start(start_loop = False)
        self.read_running_time()
        # Wait for MQTT connection
        logging.info("Waiting for MQTT connection")
        while not self.connected:
            self.mqtt_client.loop(0.1)
            self.mqtt_client.loop_misc()
        tick = 0.1  # 10 Hz refresh rate
        last_status_publish = time.time() - STATUS_PUBLISH_INTERVAL
        last_slow_update = time.time() - SLOW_UPDATE_INTERVAL
        last_fast_update = time.time() - FAST_UPDATE_INTERVAL
        data_publish_update = time.time() - DATA_PUBLISH_INTERVAL
        last_mqtt_misc = time.time()
        # MQTT main loop
        while True:
            try:
                self.mqtt_client.loop(tick)
                current_time = time.time()
                if current_time > last_status_publish + STATUS_PUBLISH_INTERVAL:
                    # Publish datasource status and links regularly
                    last_status_publish = current_time
                    self.publish_status()
                if current_time > last_fast_update + FAST_UPDATE_INTERVAL:
                    # Fast update
                    last_fast_update = current_time
                    self.update_cpu_load()
                    self.update_temperatures()
                if current_time > last_slow_update + SLOW_UPDATE_INTERVAL:
                    # Slow update
                    last_slow_update = current_time
                    self.update_ntp()
                if current_time > data_publish_update + DATA_PUBLISH_INTERVAL:
                    # Publish data
                    data_publish_update = current_time
                    self.publish_data()
                if current_time > last_mqtt_misc + 2:
                    # Let the MQTT library do its housekeeping regularly
                    last_mqtt_misc = current_time
                    self.mqtt_client.loop_misc()
            except RuntimeError:
                self.halt('Runtime error')
            except KeyboardInterrupt:
                self.halt('Keyboard interrupt')
            except BaseException as err:
                logging.error(err)


    def halt(self, reason):
        logging.debug('Exiting: ' + reason)
        self.publish_status(empty = True)
        self.save_running_time()
        self.mqtt_client.loop()
        self.stop()
        sys.exit(0)


    def read_running_time(self):
        print("Loading running time")
        self.session_start = time.time()
        try:
            f = open('running_time', 'r')
            self.accumulated_running_time = float(f.readline())
            f.close()
        except Exception as err:
            self.accumulated_running_time = 0
        print(f"Loaded accumulated running time: {self.accumulated_running_time}")


    def save_running_time(self):
        running_time = time.time() - self.session_start + self.accumulated_running_time
        try:
            f = open('running_time', 'w')
            f.write(f"{running_time}\n")
            f.close()
            logging.info(f"Saved running time value: {running_time}")
        except Exception as err:
            logging.error(f"Could not save running time counter")
            pass


    def publish_status(self, empty = False):
        links = []
        if not empty:
            for link in self.links.values():
                links.append(link.serialize())
            status = 'disconnected'
        else:
            status = 'connected'
        structure = {
            'description': "System\n\nSystem DataSource that provides various data signals about the status of computer that Octopy is running on",
            'datasource id': self.id,
            'type': 'system',
            'readonly': True,
            'status': status,
            'links': links
        }
        payload = json.dumps(structure, indent = 4)
        topic = path.join(self.topics['datasource']['status'], self.id)
        logging.debug(f"Publishing status for system datasource")
        self.publish(topic, payload)


    def publish_data(self):
        timestamp = time.time()
        if self.ntp_server:
            self.publish(self.links['ntp_synced'].value_topic, json.dumps([self.ntp_synced, {'t': timestamp}]))
            self.publish(self.links['ntp_server'].value_topic, json.dumps([self.ntp_server, {'t': timestamp}]))
            self.publish(self.links['ntp_accuracy'].value_topic, json.dumps([self.ntp_accuracy, {'t': timestamp}]))
        self.publish(self.links['cpu_frequency'].value_topic, json.dumps([self.cpu_frequency, {'t': timestamp}]))
        self.publish(self.links['cpu_load'].value_topic, json.dumps([self.cpu_load, {'t': timestamp}]))
        for sensor_name, sensor_value in self.temperatures.items():
            self.publish(self.links[sensor_name].value_topic, json.dumps([self.temperatures[sensor_name], {'t': timestamp}]))
        running_time = time.time() - self.session_start + self.accumulated_running_time
        self.publish(self.links['running_time'].value_topic, json.dumps([running_time, {'t': timestamp}]))
        uptime = time.time() - psutil.boot_time()
        self.publish(self.links['uptime'].value_topic, json.dumps([uptime, {'t': timestamp}]))


    def create_links(self):
        self.links['ntp_synced'] = SysLink({
            'value topic': path.join(self.source_prefix, 'ntp/synced'),
            'id': 'ntp_synced',
            'description': 'NTP Synced',
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'bool'
            }
        })
        self.links['ntp_server'] = SysLink({
            'value topic': path.join(self.source_prefix, 'ntp/server'),
            'id': 'ntp_server',
            'description': 'NTP Server',
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'string'
            }
        })
        self.links['ntp_accuracy'] = SysLink({
            'value topic': path.join(self.source_prefix, 'ntp/accuracy'),
            'id': 'ntp_accuracy',
            'description': 'NTP Accuracy',
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'double',
                'unit': 'ms',
                'min': 0,
                'max': 1000
            }
        })

        freq = psutil.cpu_freq()
        self.links['cpu_frequency'] = SysLink({
            'value topic': path.join(self.source_prefix, 'cpu/frequency'),
            'id': 'cpu_frequency',
            'description': 'CPU Frequency\n\nCurrent CPU frequency',
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'double',
                'unit': 'MHz',
                'min': 0,
                'max': freq.max
            }
        })
        self.links['cpu_load'] = SysLink({
            'value topic': path.join(self.source_prefix, 'cpu/load'),
            'id': 'cpu_load',
            'description': 'CPU Load\n\nCurrent CPU load',
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'double',
                'unit': '%',
                'min': 0,
                'max': 100
            }
        })

        self.links['uptime'] = SysLink({
            'value topic': path.join(self.source_prefix, 'uptime'),
            'id': 'uptime',
            'description': 'System uptime\n\nTime since system boot',
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'double',
                'unit': 's'
            }
        })
        self.links['running_time'] = SysLink({
            'value topic': path.join(self.source_prefix, 'running_time'),
            'id': 'running_time',
            'description': 'Running time\n\nTotal time that Octopy has ben running on this system since install',
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'double',
                'unit': 's'
            }
        })
        self.update_temperatures()
        for sensor_name in self.temperatures.keys():
            self.links[sensor_name] = SysLink({
                'value topic': path.join(self.source_prefix, 'temperature', sensor_name),
                'id': sensor_name,
                'description': f"Temperature sensor {sensor_name}",
                'readonly': True,
                'datasource id': self.id,
                'datainfo': {
                    'type': 'double',
                    'unit': 'C',
                    'min': 0,
                    'max': 120
                }
            })



    def update_ntp(self):
        proc = subprocess.Popen(['ntpstat'], stdout = subprocess.PIPE)
        data = proc.communicate()[0].decode('utf-8')
        if data == '':
            logging.warning(f"ntpstat returned no data. Is the NTP daemon running?")
            return
        lines = data.split("\n")
        if 'unsynchronized' in lines[0]:
            self.ntp_synced = False
            self.ntp_server = ''
            self.ntp_accuracy = 0
            logging.debug(f"Updated NTP stats: Unsynchronized!")
        else:
            self.ntp_synced = True
            self.ntp_server = re.search('\((.+)\)', lines[0]).group(1)
            self.ntp_accuracy = int(re.search('(\d+) ms', lines[1]).group(1))
            logging.debug(f"Updated NTP stats: server='{self.ntp_server}', accuracy={self.ntp_accuracy} ms")


    def update_cpu_load(self):
        freq = psutil.cpu_freq()
        self.cpu_frequency = freq.current
        self.cpu_load = psutil.cpu_percent()
        logging.debug(f"Updated CPU load stats: load={self.cpu_load} %, frequency={self.cpu_frequency} MHz")


    def update_temperatures(self):
        sensors = psutil.sensors_temperatures()
        for group_id, group_info in sensors.items():
            for sensor_info in group_info:
                sensor_name = 'temp_' + group_id
                if sensor_info.label:
                    sensor_name = sensor_name + '_' + sensor_info.label.replace(' ', '_')
                self.temperatures[sensor_name] = sensor_info.current
                logging.debug(f"System temperature sensor {sensor_name}={self.temperatures[sensor_name]} C")



if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--host',               help="MQTT host/IP", type=str, default="localhost")
    arg_parser.add_argument('--port',               help="MQTT port", type=int, default=1883)
    arg_parser.add_argument('--username', '--user', help="MQTT username", type=str, default="")
    arg_parser.add_argument('--password', '--pass', help="MQTT password", type=str, default="")
    arg_parser.add_argument('--prefix',             help="MQTT prefix", type=str, default="")
    args = arg_parser.parse_args()
    config = {
        'mqtt': {}
    }
    if args.host:
        config['mqtt']['host'] = args.host
    if args.port:
        config['mqtt']['port'] = args.port
    if args.username:
        config['mqtt']['username'] = args.username
    if args.password:
        config['mqtt']['password'] = args.password
    if args.prefix:
        config['mqtt']['prefix'] = args.prefix

    ds = SystemDatasource(config)
    ds.start()
